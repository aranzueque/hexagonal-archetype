package ${package}.${entityLowerCase}.infrastructure;

import org.springframework.beans.factory.annotation.Autowired;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import ${package}.ApplicationTestCase;
import ${package}.${entityLowerCase}.domain.${entityCamelCase};

public class ${repositoryType}${entityCamelCase}RepositoryTest extends ApplicationTestCase{
	
	@Autowired
	${repositoryType}${entityCamelCase}Repository repository;
	
	@Test
	public void save_a_valid_${entityLowerCase}() {
		assertNotNull(repository.save(${entityCamelCase}.builder().property("property-sample").build()));
	}
	
	@Test
	public void search_an_existing_${entityLowerCase}() {
		${entityCamelCase} entity${entityCamelCase} = ${entityCamelCase}.builder().property("test").build(); 
        repository.save(entity${entityCamelCase});
        assertEquals(entity${entityCamelCase}, repository.findById(entity${entityCamelCase}.getId()).get());
	}
	
	@Test
	public void search_a_non_existing_${entityLowerCase}() {
		assertFalse(repository.findById(Long.valueOf(-1)).isPresent());
	}
	
	@Test
	public void search_all_${entityLowerCase}s() {
		repository.save(${entityCamelCase}.builder().property("property-sample").build());
		assertTrue(repository.findAll().iterator().hasNext());
	}
	
	@Test
	public void update_${entityLowerCase}() {
		String original_value = "property-sample";
		String updated_value  = "property-updated";
		${entityCamelCase} entity = ${entityCamelCase}.builder().property(original_value).build();
		repository.save(entity);
		entity.setProperty(updated_value);
		repository.update(entity);
		assertEquals(updated_value, repository.findById(entity.getId()).get().getProperty());
	}
	
	@Test
	public void delete_${entityLowerCase}() {
		${entityCamelCase} entitySaved = repository.save(${entityCamelCase}.builder().property("property-sample").build());
		repository.delete(entitySaved);
		assertFalse(repository.findById(entitySaved.getId()).isPresent());
	}

}