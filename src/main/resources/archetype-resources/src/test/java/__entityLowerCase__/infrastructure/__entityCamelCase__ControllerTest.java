package ${package}.${entityLowerCase}.infrastructure;

import org.junit.Test;
import org.springframework.http.HttpMethod;
import org.springframework.test.web.servlet.MvcResult;

import ${package}.ApplicationTestCase;

public class ${entityCamelCase}ControllerTest extends ApplicationTestCase{
	
	@Test
	public void create_a_valid_${entityLowerCase}() throws Exception {
		assertRequestWithBody("POST","/${entityLowerCase}", "{\"property\":\"test\"}", 201);
	}
	
	@Test
	public void create_a_invalid_${entityLowerCase}() throws Exception {
		assertRequestWithBody("POST","/${entityLowerCase}", "{\"badproperty\":\"test\"}", 400);
	}
	
	@Test
	public void get_${entityLowerCase}s() throws Exception {
		createRandom${entityCamelCase}();
		assertRequest(HttpMethod.GET.toString(),"/${entityLowerCase}", 200);
	}

	@Test
	public void get_user_byId() throws Exception {
		String locationUri = createRandom${entityCamelCase}();
		assertRequest(HttpMethod.GET.toString(), locationUri, 200);
	}
	
	@Test
	public void update_user() throws Exception {
		String locationUri = createRandom${entityCamelCase}();
		Long id = extradIdFromLocationHeader(locationUri);
		assertRequestWithBody("PUT",locationUri, "{\"property\":\"test-updated\"}", 204);
		assertResponse(locationUri, 200,"{\"id\":"+id+", \"property\":\"test-updated\"}");
	}

	@Test
	public void delete_user() throws Exception {
		String locationUri = createRandom${entityCamelCase}();
		assertRequestExpectEmptyResponse("DELETE",locationUri, 204);
	}
	
	
	private String createRandom${entityCamelCase}() throws Exception {
		MvcResult result = assertRequestWithBody("POST","/${entityLowerCase}", "{\"property\":\"test-"+Math.random()+"\"}", 201);
		return result.getResponse().getHeader("Location");
	}
	
	private Long extradIdFromLocationHeader(String locationUri) {
		return Long.valueOf(locationUri.substring(locationUri.lastIndexOf("/")+1));
	}

}