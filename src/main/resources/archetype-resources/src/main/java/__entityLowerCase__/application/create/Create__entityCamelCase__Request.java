package ${package}.${entityLowerCase}.application.create;

import lombok.Data;
import lombok.Builder;

@Data
@Builder
public class Create${entityCamelCase}Request {
		
	private String property;
	
}

