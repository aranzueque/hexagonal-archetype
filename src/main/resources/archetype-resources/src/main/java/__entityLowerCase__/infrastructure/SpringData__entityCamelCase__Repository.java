package ${package}.${entityLowerCase}.infrastructure;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ${package}.${entityLowerCase}.domain.${entityCamelCase};

@Repository
public interface SpringData${entityCamelCase}Repository extends CrudRepository<${entityCamelCase}, Long> {

}