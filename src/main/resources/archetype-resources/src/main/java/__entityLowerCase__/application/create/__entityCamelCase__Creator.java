package ${package}.${entityLowerCase}.application.create;

import org.springframework.stereotype.Service;

import ${package}.${entityLowerCase}.domain.${entityCamelCase};
import ${package}.${entityLowerCase}.domain.${entityCamelCase}Repository;

@Service
public class ${entityCamelCase}Creator {
	
	private ${entityCamelCase}Repository repository;
	
	public ${entityCamelCase}Creator(${entityCamelCase}Repository repository) {
		this.repository = repository;
	}
	
	public ${entityCamelCase} create (Create${entityCamelCase}Request request) {
		${entityCamelCase} entity${entityCamelCase} = ${entityCamelCase}.builder().property(request.getProperty()).build();
		return repository.save(entity${entityCamelCase});
	}

}