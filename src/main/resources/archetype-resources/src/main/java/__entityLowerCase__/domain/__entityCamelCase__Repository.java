package ${package}.${entityLowerCase}.domain;

import java.util.Optional;

public interface ${entityCamelCase}Repository {

	public ${entityCamelCase} save(${entityCamelCase} ${entityLowerCase});

	public Iterable<${entityCamelCase}> findAll();
	
	public Optional<${entityCamelCase}> findById(Long id);

	public void update(${entityCamelCase} ${entityLowerCase});

	public void delete(${entityCamelCase} ${entityLowerCase});
	
}