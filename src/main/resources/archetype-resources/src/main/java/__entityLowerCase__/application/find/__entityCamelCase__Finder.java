package ${package}.${entityLowerCase}.application.find;

import java.util.Optional;

import org.springframework.stereotype.Service;

import ${package}.${entityLowerCase}.domain.${entityCamelCase};
import ${package}.${entityLowerCase}.domain.${entityCamelCase}Repository;

@Service
public class ${entityCamelCase}Finder {
	
	private ${entityCamelCase}Repository repository;
	
	public ${entityCamelCase}Finder(${entityCamelCase}Repository repository) {
		this.repository = repository;
	}
	
	public Iterable<${entityCamelCase}> findAll () {
		return repository.findAll();
	}
	
	public Optional<${entityCamelCase}> findById (Long id) {
		return repository.findById(id);
	}

}
