package ${package}.${entityLowerCase}.application.delete;

import lombok.Data;
import lombok.Builder;

@Data
@Builder
public class Delete${entityCamelCase}Request {
		
	private Long id;
	
}

