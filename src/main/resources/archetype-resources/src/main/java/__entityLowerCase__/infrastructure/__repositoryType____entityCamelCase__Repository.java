package ${package}.${entityLowerCase}.infrastructure;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ${package}.${entityLowerCase}.domain.${entityCamelCase};
import ${package}.${entityLowerCase}.domain.${entityCamelCase}Repository;

@Repository
public class ${repositoryType}${entityCamelCase}Repository implements ${entityCamelCase}Repository{

	@Autowired
	private SpringData${entityCamelCase}Repository repository;
	
	@Override
	public ${entityCamelCase} save(${entityCamelCase} entity) {
		return repository.save(entity);
	}

	@Override
	public Iterable<${entityCamelCase}> findAll() {
		return repository.findAll();
	}

	@Override
	public Optional<${entityCamelCase}> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public void update(${entityCamelCase} ${entityLowerCase}) {
		repository.save(${entityLowerCase});
	}

	@Override
	public void delete(${entityCamelCase} ${entityLowerCase}) {
		repository.delete(${entityLowerCase});		
	}

}