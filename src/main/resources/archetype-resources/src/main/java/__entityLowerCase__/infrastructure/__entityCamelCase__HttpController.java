package ${package}.${entityLowerCase}.infrastructure;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ${package}.${entityLowerCase}.application.create.Create${entityCamelCase}Request;
import ${package}.${entityLowerCase}.application.create.${entityCamelCase}Creator;
import ${package}.${entityLowerCase}.application.delete.Delete${entityCamelCase}Request;
import ${package}.${entityLowerCase}.application.delete.${entityCamelCase}Eraser;
import ${package}.${entityLowerCase}.application.find.${entityCamelCase}Finder;
import ${package}.${entityLowerCase}.application.update.${entityCamelCase}Updater;
import ${package}.${entityLowerCase}.application.update.Update${entityCamelCase}Request;
import ${package}.${entityLowerCase}.domain.${entityCamelCase};

@RestController
@RequestMapping("/${entityLowerCase}")
public class ${entityCamelCase}HttpController {
	
	@Autowired
	private ${entityCamelCase}Creator ${entityLowerCase}Creator;
	
	@Autowired
	private ${entityCamelCase}Finder ${entityLowerCase}Finder;
	
	@Autowired
	private ${entityCamelCase}Updater ${entityLowerCase}Updater;
	
	@Autowired
	private ${entityCamelCase}Eraser ${entityLowerCase}Eraser;
	
	@GetMapping("/{id}")
	public ResponseEntity<${entityCamelCase}HttpResponse> get${entityCamelCase}(@PathVariable Long id) {
		Optional<${entityCamelCase}> ${entityLowerCase} = ${entityLowerCase}Finder.findById(id);
		return ${entityLowerCase}.isPresent()?
				new ResponseEntity<>(map${entityCamelCase}ToHttpResponseBody(${entityLowerCase}), HttpStatus.OK):
				new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
	
	@GetMapping
	public ResponseEntity<List<${entityCamelCase}HttpResponse>> get${entityCamelCase}s() {
		Iterable<${entityCamelCase}> all${entityCamelCase}s = ${entityLowerCase}Finder.findAll();
		List<${entityCamelCase}HttpResponse> response = map${entityCamelCase}ListToHttpResponseBody(all${entityCamelCase}s);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

	@PostMapping
	public ResponseEntity<${entityCamelCase}HttpResponse> create${entityCamelCase}(@Valid @RequestBody ${entityCamelCase}HttpRequest request) {
		${entityCamelCase} ${entityLowerCase}Created = ${entityLowerCase}Creator.create(
				Create${entityCamelCase}Request.builder()
				.property(request.getProperty())
				.build());
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", "/${entityLowerCase}/"+${entityLowerCase}Created.getId());
		return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
	
	@PutMapping("/{id}")
	public ResponseEntity<${entityCamelCase}HttpResponse> update${entityCamelCase}(@PathVariable Long id, @RequestBody ${entityCamelCase}HttpRequest request) {
		${entityLowerCase}Updater.update(Update${entityCamelCase}Request.builder().id(id).property(request.getProperty()).build());
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
	
	@DeleteMapping("/{id}")
	public ResponseEntity<${entityCamelCase}HttpResponse> delete${entityCamelCase}(@PathVariable Long id) {
		${entityLowerCase}Eraser.delete(Delete${entityCamelCase}Request.builder().id(id).build());
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

	//**
	//*  Mappings to decouple 'http' protocol response from application service response 
	//**
	
	private ${entityCamelCase}HttpResponse map${entityCamelCase}ToHttpResponseBody(Optional<${entityCamelCase}> ${entityLowerCase}) {
		${entityCamelCase}HttpResponse response = new ${entityCamelCase}HttpResponse();
		if (${entityLowerCase}.isPresent()) {
			response.setId(${entityLowerCase}.get().getId());
			response.setProperty(${entityLowerCase}.get().getProperty());
		}
		return response;
	}
	
	private List<${entityCamelCase}HttpResponse> map${entityCamelCase}ListToHttpResponseBody(Iterable<${entityCamelCase}> all${entityCamelCase}s) {
		List<${entityCamelCase}> all${entityCamelCase}sList = new ArrayList<>();
		all${entityCamelCase}s.forEach(all${entityCamelCase}sList::add);
        return all${entityCamelCase}sList.stream().map(temp -> {
        	${entityCamelCase}HttpResponse ${entityLowerCase}Response = new ${entityCamelCase}HttpResponse();
        	${entityLowerCase}Response.setId(temp.getId());
        	${entityLowerCase}Response.setProperty(temp.getProperty());
            return ${entityLowerCase}Response;
        }).collect(Collectors.toList());
	}

}

//**
//*  API objects could be moved to an isolated api doc package as a good practice
//**
final class ${entityCamelCase}HttpRequest {
	
	private Long id;
	
	@NotNull(message = "property is mandatory")
	private String property;

	public String getProperty() {
		return property;
	}
	
	public Long getId() {
		return id;
	}

	public void setProperty(String property) {
		this.property = property;
	}	
}

//**
//*  API objects could be moved to an isolated api doc package as a good practice
//**
final class ${entityCamelCase}HttpResponse {
	
	private Long id;
	
	private String property;

	public String getProperty() {
		return property;
	}
	
	public void setProperty(String property) {
		this.property = property;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
}
