package ${package}.${entityLowerCase}.application.update;

import lombok.Data;
import lombok.Builder;

@Data
@Builder
public class Update${entityCamelCase}Request {
		
	private Long id;
	private String property;
	
}