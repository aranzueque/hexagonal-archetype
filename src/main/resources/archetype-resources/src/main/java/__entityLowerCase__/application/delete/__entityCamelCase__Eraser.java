package ${package}.${entityLowerCase}.application.delete;

import org.springframework.stereotype.Service;

import ${package}.${entityLowerCase}.domain.${entityCamelCase};
import ${package}.${entityLowerCase}.domain.${entityCamelCase}Repository;

@Service
public class ${entityCamelCase}Eraser {
	
	private ${entityCamelCase}Repository repository;
	
	public ${entityCamelCase}Eraser(${entityCamelCase}Repository repository) {
		this.repository = repository;
	}
	
	public void delete (Delete${entityCamelCase}Request request) {
		${entityCamelCase} entity${entityCamelCase} = ${entityCamelCase}.builder().id(request.getId()).build();
		repository.delete(entity${entityCamelCase});
	}

}