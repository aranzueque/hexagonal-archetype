package ${package}.${entityLowerCase}.application.update;

import org.springframework.stereotype.Service;

import ${package}.${entityLowerCase}.domain.${entityCamelCase};
import ${package}.${entityLowerCase}.domain.${entityCamelCase}Repository;

@Service
public class ${entityCamelCase}Updater {
	
	private ${entityCamelCase}Repository repository;
	
	public ${entityCamelCase}Updater(${entityCamelCase}Repository repository) {
		this.repository = repository;
	}
	
	public void update (Update${entityCamelCase}Request request) {
		${entityCamelCase} entity${entityCamelCase} = ${entityCamelCase}.builder().id(request.getId()).property(request.getProperty()).build();
		repository.update(entity${entityCamelCase});
	}

}