# Sngular KickStarter for Backend proyects

Desde el grupo **Standars** de Sngular queremos impulsar diferentes herramientas para la creación aplicaciones de forma **rápida**, pero garantizando los **máximos niveles de calidad**. 

Para ello se ha desarrollado esta plantilla, que nos ayudará a generar **proyectos backend** basados en Java/Maven/SpringBoot con una base de desarrollo común, **acelerando la puesta en marcha en fases iniciales**. El proyecto generado a partir de esta plantilla nos proveerá de:

* Todas las **dependencias** base necesarias
* Una **estructura de carpetas** para organizar tu proyecto de forma eficiente
* **Configuración** independiente por **entornos**
* Entorno de desarrollo configurado con H2 en memoria y consola disponible
* **Propiedades** por entorno con ejemplos de uso de **variables de sistema**
* Propuesta de **estrategia de testing** básica
con ejemplos de implementación de pruebas unitarias y de aceptación
* **Métricas** de rendimiento
* **Control** y monitorización de **calidad**
* **CRUD** básico sobre tu entidad de negocio
* **Documentación** de la **API** siguiendo estandar OpenAPI v3
* **Docker ready!** Despliega donde quieras

### Pre-requisitos 📋

Para poder comenzar a trabajar con este arquetipo necesitar unos conocimientos minimos de:

* Java 8+
* Maven v3+
* Spring Boot v2+

Si no te sientes con suficiente confianza en alguna de ellas, puedes realizar los siguientes cursos en Udemy o revisar la documentación oficial:

* https://sngular.udemy.com/course/apachemaven
* https://sngular.udemy.com/course/universidad-java-especialista-en-java-desde-cero-a-master/
* https://docs.spring.io/spring-boot/docs/current/reference/html/using-spring-boot.html#using-boot

### Instalación 🔧

Para poder comenzar necesitas tener instalado en tu equipo:

* Java 8+
* Apache Maven 3+

1. Configura maven para que disponga de acceso al repositorio de artefactos de Sngular `https://tools.sngular.com/nexus/repository`. Si necesitas ayuda, sigue los pasos aquí descritos: `https://blog.sonatype.com/using-nexus-3-as-your-repository-part-1-maven-artifacts` para configurar tu cliente.

> _Si quieres disponer del arquetipo en tu repositorio local, clona el proyecto desde el repositorio de git de Standards(...) e instalalo con un simple `mvn install`_

2. Para poder generar tu primer proyecto, desde el raiz de tu workspace, solo debes ejecutar el siguiente comando de maven

```
mvn archetype:generate 
-DarchetypeGroupId=com.sngular
-DarchetypeArtifactId=hexagonal-archetype
-DarchetypeVersion=0.0.1-SNAPSHOT 
-DgroupId=    {must be provided - your project group id}
-DartifactId= {must be provided - your project artifact id}
-Dversion=    {must be provided - your project initial version}

```

3. El generador te preguntará por una entidad para poder generar su CRUD correspondiente, algún ejemplo podría ser producto, pedido, cuenta..

```
Define value for property 'entity': 
```

4. Pulsa intro cuando te pregunte por el formato en camel y lowercase. Estos valores los utilizara velocity para generar, utilizando el formato correcto, los nombres de paqueterías, clases, variables, etc
```
Define value for property 'entityCamelCase' ...
Define value for property 'entityLowerCase' ...
```
5. Confirma la configuración pulsando intro o modifícala en caso necesario
```
Confirm properties configuration: ...
 Y:
```
5.a En caso de querer modificar la configuración, podrás personalizar
```
groupId: 
artifactId: 
version: 
package: 
entity: 
entityCamelCase: 
entityLowerCase: 
javaVersion: 
repositoryType: InMemory (por el momento solo esta soportado InMemory)
```

6. El proyecto ya se ha creado y esta listo para poder ser importado como proyecto maven desde tu IDE favorito. Enjoy!!

## Ejecutando el proyecto ⚙️

1. Para poder ejecutar la aplicación, basta con lanzar desde el directorio raiz del proyecto el siguiente comando
 ```
mvn spring-boot:run -Dspring.profiles.active=dev -Dlocal.h2.username=admin -Dlocal.h2.password=admin
```
Los parametros indican:
* `spring.profiles.active` Perfil que quieres activar. Esto nos permite cargar diferentes configuraciones en función de las necesidades, configuraciones por entorno, etc..
* `local.h2.username` Username para acceder al H2 que se levanta en memoria
* `local.h2.password` Password para acceder al H2 que se levanta en memoria

Una vez levantada la aplicación dispondremos de los siguientes endpoints

* GET `http://localhost:8080/api/{ENTITY}`
* GET  `http://localhost:8080/api/{ENTITY}/{ENTITY_ID}`
* POST `http://localhost:8080/api/{ENTITY}`
* PUT  `http://localhost:8080/api/{ENTITY}/{ENTITY_ID}`
* DELETE `http://localhost:8080/api/{ENTITY}/{ENTITY_ID}`

También dispondremos de acceso a la consola de H2 en memoria

* H2-CONSOLE `http://localhost:8080/api/h2-console`

La documentación de la API generada estará disponible igualmente

* OPENAPI v3 `http://localhost:8080/api/v3/api-docs/`
* SWAGGER `http://localhost:8080/api/swagger-ui/index.html?configUrl=/api/v3/api-docs/swagger-config`

Las métricas junto el health-check estarán habilitados mediante Actuator en los siguientes endpoints

* `http://localhost:8080/api/actuator/health` 
* `http://localhost:8080/api/actuator/metrics`
* `http://localhost:8080/api/actuator/metrics/{requiredMetricName}`

Para la calidad de código, utilizaremos sonar, solo disponible al levantar docker-compose (ver capítulo "Despliegue en contenedores" )
* `http://localhost:9000`

## Ejecutando las pruebas ⚙️

Para ejecutar las pruebas solo es necesario lanzar el comando habitual de maven para ello
```
mvn test
```
Es importante comprender la estrategia de testing propuesta, para ello por favor revisa el articulo ubicado en Standards que lo detalla (add link here!!!)

### Y las pruebas de estilo de codificación ⌨️

Para poder ejecutar el control de calidad de código debemos haber levantado previamente la imagen de sonarqube (Ver siguiente apartado, "Despliegue en contenedores" ) o haber instalado sonar lint en nuestro IDE (https://www.sonarlint.org) 

```
mvn sonar:sonar -Dsonar.login=admin -Dsonar.password=admin -Dsonar.host.url=http://localhost:9000
```
Recuerda que si quieres que las métricas de cobertura aparezcan en el informe, el plugin de jacoco debe haber generado el contenido antes de que sonar lo intente procesar. Para ello es suficiente con realizar el empaquetado del proyecto
```
mvn package
``` 
Si quieres comprobar que todo esta correcto, revisa que en la carpeta /target se haya generado una subcarpeta /site

## Despliegue en contenedores 📦

Para poder desplegar tu contenedor, lo primero que deber hacer es construir tu aplicación. Si has realizado la evaluación de cobertura de código, ya tendrás el empaquetado listo, por lo que no sería necesario ejecutar de nuevo este comando.
```
mvn package
```
Una vez construida, vamos a generar el contenedor asignándole una etiqueta con su versión correspondiente, para ello ejecuta el siguiente comando desde el directorio raíz del proyecto. Recuerda utilizar la misma etiqueta y versión que referencies en tu fichero de configuración de docker-compose
```
docker build -t sample-tag:latest .
```
Una vez disponemos de la imagen de docker, podemos levantar el la aplicación desde el contenedor junto con el resto de dependencias que tengamos registradas en nuestro docker compose, ejecutando 
```
docker compose up
```

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Java](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) - El lenguaje de programación
* [SpringBoot](https://docs.spring.io/spring-boot/docs/current/reference/html/) - El framework de desarrollo
* [Maven](https://maven.apache.org/) - Gestor de ciclo de vida de desarrollo
* [Docker](https://www.docker.com/get-started) - Para la gestión de contenedores
* [Sawgger](https://swagger.io/specification/) - Desarrollo y documentacion de RESTful APIs
* [SonarQube](https://www.sonarqube.org/) - Control de calidad y buenas practicas


## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestro site  [Wiki](https://standards.sngular.com/es/)

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://gitlab.sngular.com/standards/code/BACK/JAVA/standards-back-arquetipe-java-spring-maven).

## Autores ✒️

* **Rafael Aranzueque Baena** - *Trabajo Inicial* 
* **Antonio Alvarez Huerta** - *Trabajo Inicial* 

## Licencia 📄

Este proyecto está bajo la Licencia (Pending..) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.

---
⌨️ con ❤️ desde [Sngular](https://www.sngular.com) 😊